package com.example.lleon.demo2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by lleon on 2016/9/14.
 */
public class webViewer extends Activity {
    private WebView webView;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("WEBSIDE","ZZZZZZZZ1");
        setContentView(R.layout.activity_web);
        Log.i("WEBSIDE","ZZZZZZZZ2");
        Intent url_get=getIntent();
        Log.i("WEBSIDE","ZZZZZZZZ");
        String url=url_get.getStringExtra("webjumper");

        Log.i("WEBSIDE","zzzzzzzzz");
        webView=(WebView)findViewById(R.id.wb_content);

        startReadUrl(url);
        Log.i("WEBSIDE",url);
    }

    public void startReadUrl(String url) {// btn_login的触发事件 点击后 webView开始读取url
        // TODO Auto-generated method stub
        Log.i("WEBSIDE","zzzzzzzzz1");
        webView.loadUrl(url);// webView加载web资源
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {// 覆盖webView默认通过系统或者第三方浏览器打开网页的行为
                // 如果为false调用系统或者第三方浏览器打开网页的行为
                // TODO Auto-generated method stub
                view.loadUrl(url);// webView加载web资源
                return true;
            }
        });
        Log.i("WEBSIDE","zzzzzzzzz2");
        WebSettings settings = webView.getSettings();// 启用支持javascript
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);// web加载页面优先使用缓存加载
        webView.getSettings().setBlockNetworkImage(false);

        webView.setWebChromeClient(new WebChromeClient(){//设置网页加载进度条
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if(newProgress==100){
                    closeDialog();
                }else{
                    openDialog(newProgress);
                }
            }
            private void closeDialog(){
                if(dialog!=null && dialog.isShowing()){
                    dialog.dismiss();
                    dialog = null;
                }
            }
            private void openDialog(int newProgress){
                if(dialog == null){
                    dialog = new ProgressDialog(webViewer.this);
                    dialog.setTitle("Loading");
                    dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    dialog.setProgress(newProgress);
                    dialog.show();
                }else {
                    dialog.setProgress(newProgress);
                }
            }
        });
        Log.i("WEBSIDE","zzzzzzzzz3");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode == KeyEvent.KEYCODE_BACK){
            if(webView.canGoBack()){
                webView.goBack();
                return true;
            }else{
                System.exit(0);
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}