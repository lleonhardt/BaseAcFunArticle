package com.example.lleon.demo2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private ListView mListView;
    private TextView mTextView;
    private String[] Item = {"综合", "工作·情感", "动画文化", "漫画·轻小说", "游戏"};

    private static String url = "http://139.199.189.90:8080/test/";
    Date date = new Date();
    String dates = new SimpleDateFormat("yyMMdd").format(date);
    private String cpURL = url + "complex/" + dates;
    private String wkURL = url + "work/" + dates;
    private String anURL = url + "animation/" + dates;
    private String cmURL = url + "comic/" + dates;
    private String gmURL = url + "game/" + dates;
    private String[] Urls={ cpURL, wkURL, anURL, cmURL, gmURL };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mListView=(ListView)findViewById(R.id.lv_content);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final TextView url_tv=(TextView) view.findViewById(R.id.tv_url);
                Intent jump=new Intent(MainActivity.this,webViewer.class);
                jump.putExtra("webjumper",url_tv.getText().toString());
                startActivity(jump);
            }
        });
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final TextView bananas_tv=(TextView) view.findViewById(R.id.tv_bananas);
                final TextView views_tv=(TextView) view.findViewById(R.id.tv_views);
                final TextView comments_tv=(TextView) view.findViewById(R.id.tv_comments);
                Toast.makeText(MainActivity.this, "香蕉:" + bananas_tv.getText().toString() +
                        "  围观:" + views_tv.getText().toString() +
                        "  评论:" + comments_tv.getText().toString(), Toast.LENGTH_SHORT).show();
                return true;              //返回true将不会触发OnItemClickListener
            }
        });
        mTextView = (TextView)findViewById(R.id.tv_content);

        setDefault(0);                   //设置默认主页
        Log.d("Time",dates);
    }

    private void setDefault(int arg){
        mTextView.setText(Item[arg]);
        new ArticleAsyncTask().execute(Urls[arg]);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            mTextView.setText(Item[0]);
            new ArticleAsyncTask().execute(cpURL);
        } else if (id == R.id.nav_gallery) {
            mTextView.setText(Item[1]);
            new ArticleAsyncTask().execute(wkURL);
        } else if (id == R.id.nav_slideshow) {
            mTextView.setText(Item[2]);
            new ArticleAsyncTask().execute(anURL);
        } else if (id == R.id.nav_manage) {
            mTextView.setText(Item[3]);
            new ArticleAsyncTask().execute(cmURL);
        } else if (id == R.id.nav_share) {
            mTextView.setText(Item[4]);
            new ArticleAsyncTask().execute(gmURL);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*
    * 创建AsyncTask用于请求网络数据加载
    * */
    class ArticleAsyncTask extends AsyncTask<String, Void, List<ArticleBean>> {
        @Override
        protected List<ArticleBean> doInBackground(String... params) {
            try {
                Log.d("tag", "tag");
                return getJsonData(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(List<ArticleBean> articleBeans) {
            super.onPostExecute(articleBeans);
            /*
            * 实例化Adapter，为ListView填充数据
            * */
            ArticleAdapter adapter = new ArticleAdapter(MainActivity.this, articleBeans);
            mListView.setAdapter(adapter);
        }
    }
    /*
    * 根据url来获取并解析JSON数据
    * */
    private List<ArticleBean> getJsonData(String url) throws IOException {
        List<ArticleBean> articleBeanList = new ArrayList<>();
        Log.d("tag", "tag3");
        /*
        * 获取JSON并解析JSON
        *
        * JSON数据格式为：
        * {"pages":[
        *   {"page-id":"3316198","url":"http://www.acfun.tv/a/ac3316198","title":"8分钟永久恢复视力 比普通人视力增强3倍","ac-id":"ac3316198","onlooker":74109,"comments":370,"banana":5},
        *   {"page-id":"3316800","url":"http://www.acfun.tv/a/ac3316800","title":"东莞运钞车被砸案细节公开：押运员多次警告别砸车","ac-id":"ac3316800","onlooker":52933,"comments":1236,"banana":1},
        *   {"page-id":"3316254","url":"http://www.acfun.tv/a/ac3316254","title":"中国核聚变获重大突破 燃烧温度比太阳都高破纪录","ac-id":"ac3316254","onlooker":66115,"comments":517,"banana":16},
        *   {"page-id":"3317624","url":"http://www.acfun.tv/a/ac3317624","title":"小鲜肉被曝订做“人皮面具” 只为演戏便于用替身","ac-id":"ac3317624","onlooker":46731,"comments":217,"banana":1},
        *   {"page-id":"3317164","url":"http://www.acfun.tv/a/ac3317164","title":"120岁世界最长寿男人：至今仍是“童子身”","ac-id":"ac3317164","onlooker":63719,"comments":219,"banana":21},
        *   {"page-id":"3316642","url":"http://www.acfun.tv/a/ac3316642","title":"日媒：外国人为什么不觉得中国菜好吃？","ac-id":"ac3316642","onlooker":135730,"comments":1358,"banana":21},
        *   {"page-id":"3317111","url":"http://www.acfun.tv/a/ac3317111","title":"王思聪开撕权志龙：就几个纹身显摆！滚回韩国去","ac-id":"ac3317111","onlooker":106119,"comments":804,"banana":6},
        *   {"page-id":"3317263","url":"http://www.acfun.tv/a/ac3317263","title":"东北爷们遇南方蟑螂立马怂？一条微博引发集体共鸣","ac-id":"ac3317263","onlooker":73138,"comments":759,"banana":9},
        *   {"page-id":"3316421","url":"http://www.acfun.tv/a/ac3316421","title":"如果发生战争，哪些现代国家的军队绝对不能低估？ [美国媒体]","ac-id":"ac3316421","onlooker":45157,"comments":383,"banana":4}
        *   ]}
        * */
        String jsonString;
        try {
            Log.d("tag", "tag4");
            jsonString = readStream(new URL(url).openStream());
            JSONObject jsonObject;
            ArticleBean articleBean;
            jsonObject = new JSONObject(jsonString);
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            /*
            * 逐行解析JSON数据
            * */
            for(int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                articleBean = new ArticleBean();

                articleBean.acTitle = jsonObject.getString("title");
                articleBean.acId = jsonObject.getString("aid");

                articleBean.acUrl = jsonObject.getString("url");
                articleBean.acViews = jsonObject.getString("views");
                articleBean.acBananas = jsonObject.getString("bananas");
                articleBean.acComments = jsonObject.getString("comments");

                articleBeanList.add(articleBean);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("tag", "tag6");
        return articleBeanList;
    }

    /*
    * 读取输入数据流
    * */
    private String readStream(InputStream is) {
        Log.d("tag", "tag5");
        InputStreamReader isr;
        String result = "";
        try {
            String line = "";
            isr = new InputStreamReader(is, "utf-8");
            BufferedReader br = new BufferedReader(isr);
            while((line = br.readLine()) != null) {
                result += line;
            }
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}